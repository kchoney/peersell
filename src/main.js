import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"
import Carousel3d from 'vue-carousel-3d'

createApp(App).use(router, Carousel3d).mount('#app')
